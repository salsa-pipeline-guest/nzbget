Source: nzbget
Section: net
Priority: optional
Maintainer: Andreas Moog <andreas.moog@warperbbs.de>
Build-Depends:
 debhelper (>= 11),
 libncurses5-dev,
 libssl-dev,
 libxml2-dev,
 pkg-config,
 zlib1g-dev
Standards-Version: 4.2.1
Homepage: https://nzbget.net
Vcs-Git: https://salsa.debian.org/amoog-guest/nzbget.git
Vcs-Browser: https://salsa.debian.org/amoog-guest/nzbget/commits/master

Package: nzbget
Architecture: any
Depends:
 libjs-elycharts (>= 2.1.5),
 libjs-raphael (>= 2.1.0),
 ${misc:Depends},
 ${shlibs:Depends}
Recommends: python3, unrar-free | unrar
Suggests: par2
Description: command-line based binary newsgrabber for nzb files
 NZBGet is a command-line based binary newsgrabber that uses nzb files as
 described in http://docs.newzbin.com/index.php/Newzbin:NZB_Specs, they are
 commonly used to describe binaries posted in the Usenet that span multiple
 posts.
 .
 nzbget can be run as a standalone tool, in client/server mode or as a daemon,
 which makes it ideal to run on NAS-devices or routers. nzbget supports
 automatic par-check/-repair can use scripts to postprocess downloaded files,
 for example to stream them to an audio/video player.

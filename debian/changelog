nzbget (21.0~r2296+dfsg-1) experimental; urgency=medium

  * New upstream development version
  * Use bundled bootstrap 2.0.3 for Web UI (Closes: #908427)
    Will use Debian's bootstrap 3 when nzbget supports it

 -- Andreas Moog <andreas.moog@warperbbs.de>  Sat, 16 Feb 2019 21:44:55 +0100

nzbget (21.0~r2220+dfsg-2) experimental; urgency=medium

  * Mention correct location for user defined systemd service file
    Thanks to Bruno Kleinert for the patch. (Closes: #917087)

 -- Andreas Moog <andreas.moog@warperbbs.de>  Fri, 11 Jan 2019 11:42:10 +0100

nzbget (21.0~r2220+dfsg-1) experimental; urgency=medium

  * New upstream development version

 -- Andreas Moog <andreas.moog@warperbbs.de>  Sun, 09 Sep 2018 20:35:06 +0200

nzbget (20.0+dfsg-1) unstable; urgency=medium

  * New Upstream release

 -- Andreas Moog <andreas.moog@warperbbs.de>  Thu, 21 Jun 2018 10:11:40 +0200

nzbget (20.0~r2181+dfsg-2) experimental; urgency=medium

  * Build with -fpermissive on arm and armhf
  * Add basic autopkgtest

 -- Andreas Moog <andreas.moog@warperbbs.de>  Sat, 26 May 2018 18:54:48 +0200

nzbget (20.0~r2181+dfsg-1) experimental; urgency=medium

  * New upstream development version
  * Use Files-Excluded, remove get-orig-source target
  * Compliant with Policy 4.1.4
  * Use dh compat 11

 -- Andreas Moog <andreas.moog@warperbbs.de>  Mon, 14 May 2018 22:16:19 +0200

nzbget (20.0~r2171+dfsg-1) experimental; urgency=medium

  * New upstream development version
  * Use salsa.debian.org for VCS-Git/Browser
  * Remove unneccesary --parallel option

 -- Andreas Moog <andreas.moog@warperbbs.de>  Tue, 03 Apr 2018 21:00:12 +0200

nzbget (20.0~r2159+dfsg-1) experimental; urgency=medium

  * New upstream development version

 -- Andreas Moog <andreas.moog@warperbbs.de>  Tue, 14 Nov 2017 20:57:09 +0100

nzbget (20.0~r2147+dfsg-1) experimental; urgency=medium

  * New upstream development version
  * Update sample configuration file
  * Use Priority: optional
  * Remove all trailing whitespaces

 -- Andreas Moog <andreas.moog@warperbbs.de>  Tue, 31 Oct 2017 11:57:11 +0100

nzbget (20.0~r2108+dfsg-2) experimental; urgency=medium

  * Add unrar as alternate recommends

 -- Andreas Moog <andreas.moog@warperbbs.de>  Sat, 30 Sep 2017 19:49:13 +0200

nzbget (20.0~r2108+dfsg-1) experimental; urgency=medium

  * New upstream development version
  * Compliant with Policy 4.1.1
  * Update sample configuration file
  * Remove useless dh-autoreconf build depends

 -- Andreas Moog <andreas.moog@warperbbs.de>  Fri, 29 Sep 2017 16:25:41 +0200

nzbget (19.1+dfsg-1) unstable; urgency=medium

  * New upstream version 19.1

 -- Andreas Moog <andreas.moog@warperbbs.de>  Sun, 09 Jul 2017 19:26:09 +0200

nzbget (19.1~r2031+dfsg-1) experimental; urgency=medium

  * New upstream development version 19.1~r2031
  * Remove auto-generated cruft for Windows/OSX from tarball

 -- Andreas Moog <andreas.moog@warperbbs.de>  Sat, 08 Jul 2017 18:26:51 +0200

nzbget (19.0+dfsg-1) unstable; urgency=medium

  * New upstream version 19.0+dfsg
  * Declare compliance with Policy 4.0.0
  * Update sample configuration file
  * Refresh patches
  * Use pkg-info.mk

 -- Andreas Moog <andreas.moog@warperbbs.de>  Thu, 29 Jun 2017 20:44:25 +0200

nzbget (18.1+dfsg-5) unstable; urgency=medium

  * Upload to unstable
  * Clarify steps to enable daemon in debian/README.Debian instead of using
    dh-sysuser.

 -- Andreas Moog <andreas.moog@warperbbs.de>  Sun, 18 Jun 2017 13:12:14 +0200

nzbget (18.1+dfsg-4) experimental; urgency=medium

  * debian/control, d/nzbget.service, d/rules, d/nzbget.postinst:
    - Use dh-sysuser to handle user creation
    - Adjust permissions for log-directory
  * debian/nzbget.conf:
    - Adjust default paths to work with the auto-created user
  * debian/copyright:
    - Update copyright information, clarify license for catch.h
  * debian/README.Debian:
    - Add information on how to enable the systemd-service
  * debian/compat:
    - Use dh compat level 10
  * debian/watch:
    - Update to not show beta releases

 -- Andreas Moog <andreas.moog@warperbbs.de>  Sun, 21 May 2017 17:29:01 +0200

nzbget (18.1+dfsg-3) experimental; urgency=medium

  * Remove unrar-free from Recommends, unfortunately it is not a drop-in
    replacement and has different command-line options.
  * Add a note to README.Debian about installing additional archivers.

 -- Andreas Moog <andreas.moog@warperbbs.de>  Wed, 17 May 2017 19:58:51 +0200

nzbget (18.1+dfsg-2) experimental; urgency=medium

  * Update sample configuration file
  * Add unrar-free to Recommends, unrar should be present in almost all
    circumstances.

 -- Andreas Moog <andreas.moog@warperbbs.de>  Tue, 16 May 2017 23:37:44 +0200

nzbget (18.1+dfsg-1) experimental; urgency=medium

  * New Upstream Release
  * Remove patch for OpenSSL 1.1 compatibility, integrated upstream
  * Refreshed patches
  * Add hardening buildflags

 -- Andreas Moog <andreas.moog@warperbbs.de>  Wed, 10 May 2017 21:45:38 +0200

nzbget (17.1+dfsg-3) unstable; urgency=medium

  * Add patch from upstream git for OpenSSL 1.1 compatibility
  * Add zlib1g-dev to Build-Depends
  * Correct VCS Url

 -- Andreas Moog <andreas.moog@warperbbs.de>  Thu, 03 Nov 2016 18:34:43 +0100

nzbget (17.1+dfsg-2) unstable; urgency=medium

  * Update copyright file to specifically mention OpenSSL linking exemption
  * Update homepage field

 -- Andreas Moog <andreas.moog@warperbbs.de>  Sat, 22 Oct 2016 16:34:49 +0200

nzbget (17.1+dfsg-1) unstable; urgency=medium

  * New Upstream Release
  * Update standards version, no further changes needed
  * Build with Openssl
  * Use https-URI for VCS-entries
  * Remove ununsed lintian-override

 -- Andreas Moog <andreas.moog@warperbbs.de>  Thu, 20 Oct 2016 19:57:27 +0200

nzbget (17.0+dfsg-1) unstable; urgency=medium

  * New Upstream Release
  * debian/nzbget.conf:
    - Add WebDir and ConfigTemplate to sample configuration (LP: #1576471)
    - Update sample configuration file
  * debian/patches: Refresh patches
  * debian/rules: Use GnuTLS

 -- Andreas Moog <andreas.moog@warperbbs.de>  Sun, 31 Jul 2016 15:13:43 +0200

nzbget (16.4+dfsg-1) unstable; urgency=medium

  * New Upstream Release

 -- Andreas Moog <andreas.moog@warperbbs.de>  Sat, 06 Feb 2016 12:03:47 +0100

nzbget (16.0+dfsg-3) unstable; urgency=medium

  * debian/source/lintian-overrides:
    - Add override for tag "source-is-missing" (for file webui/upload.js)
      The file IS the source.

 -- Andreas Moog <andreas.moog@warperbbs.de>  Sat, 24 Oct 2015 13:04:49 +0200

nzbget (16.0+dfsg-2) unstable; urgency=medium

  * Upload to unstable

 -- Andreas Moog <andreas.moog@warperbbs.de>  Sun, 18 Oct 2015 13:30:45 +0200

nzbget (16.0+dfsg-1) unstable; urgency=medium

  * New Upstream Release
  * Use included jquery library, nzbget isn't yet compatible with versions
    newer than 1.7.2 (Closes: #799126)
  * debian/watch: Update to point to github
  * debian/nzbget.conf
    - Update sample configuration file
  * debina/docs
    - Remove NEWS, no longer existing

 -- Andreas Moog <andreas.moog@warperbbs.de>  Sat, 17 Oct 2015 20:48:50 +0200

nzbget (15.0+dfsg-1) unstable; urgency=medium

  * New Upstream Release (Closes: #786437)
  * debian/patches:
    - Remove 0011-fix-potential-buffer-overflow.patch, no longer needed
    - Refresh patches
  * debian/nzbget.conf
    - Update sample configuration file to include new options introduced by
      new upstream version.

 -- Andreas Moog <andreas.moog@warperbbs.de>  Fri, 12 Jun 2015 20:52:22 +0200

nzbget (14.2+dfsg-2) unstable; urgency=medium

  * Upload to unstable
  * This release fixes errors with gnutls while running in daemon mode
    (Closes: #767169)

 -- Andreas Moog <andreas.moog@warperbbs.de>  Fri, 01 May 2015 12:31:55 +0200

nzbget (14.2+dfsg-1) experimental; urgency=medium

  * New Upstream release

 -- Andreas Moog <andreas.moog@warperbbs.de>  Sat, 28 Feb 2015 19:38:17 +0100

nzbget (14.1+dfsg-1) experimental; urgency=medium

  * New Upstream release (Closes: #768863)
  * debian/patches:
    - Remove 0010_unnecessary_gcryptdep.patch, included upstream
    - Refresh remaining patches
  * debian/control:
    - Remove no longer needed build-depends on libpar2-dev and libsigc++-dev
  * debian/nzbget.conf
    - Update sample configuration file to include new options introduced by
      new upstream version.

 -- Andreas Moog <andreas.moog@warperbbs.de>  Thu, 25 Dec 2014 12:58:06 +0100

nzbget (13.0+dfsg-1) unstable; urgency=medium

  * Acknowledge NMU (Closes: #758286)
    - Big thanks to Andreas Metzler for the upload
  * New upstream release (Closes: #763342)
  * debian/patches:
    - Refresh 0001-dont-embed-libraries.patch
    - Refresh 0010_unnecessary_gcryptdep.patch
    - Add 0011-fix-potential-buffer-overflow.patch to fix gcc warning
      about a potential buffer overflow.
  * debian/control:
    - Add dependency for libjs-raphael and -elycharts, needed for the web
      interface.
    - Run 'wrap-and-sort -s'
    - Update standards version, no further changes needed
  * debian/nzbget.links:
    - Also use system libjs-raphael and -elycharts libraries
  * debian/nzbget.conf
    - Update sample configuration file to include new options introduced by
      new upstream version.
  * debian/copyright
    - Update years
    - Include note about all javascript libraries that are excluded in the
      source tarball.

 -- Andreas Moog <andreas.moog@warperbbs.de>  Sun, 12 Oct 2014 18:40:14 +0200

nzbget (12.0+dfsg-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rebuild against GnuTLS v3. Closes: #753137
  * 0010_unnecessary_gcryptdep.diff: Only link against gcrypt if
    gnutls << 2.12. Closes: #745958

 -- Andreas Metzler <ametzler@debian.org>  Sat, 16 Aug 2014 11:53:28 +0200

nzbget (12.0+dfsg-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    - Change maintainer email address
    - Update standards version to 3.9.5, no changes needed
  * debian/rules:
    - enable parallel building
  * debian/patches:
    - Refresh 0001-dont-embed-libraries.patch
    - Remove 0002-hardening-buffer-overflow.patch, included upstream
  * debian/nzbget.install:
    - Install sample configuration file in webui folder to serve as template
      in the web interface
  * debian/nzbget.conf:
    - Update sample configuration file to include new options introduced by
      the new upstream version

 -- Andreas Moog <andreas.moog@warperbbs.de>  Sun, 12 Jan 2014 13:44:33 +0100

nzbget (11.0+dfsg-2) unstable; urgency=low

  * Upload to unstable
  * debian/patches/0002-hardening-buffer-overflow.patch: (Closes: #720099)
    - Fix potential buffer overflow in RemoteClient.cpp

 -- Andreas Moog <amoog@ubuntu.com>  Sun, 25 Aug 2013 19:32:30 +0200

nzbget (11.0+dfsg-1) experimental; urgency=low

  * New upstream release (Closes: #701896)
  * Repackage original tarball to remove copies of jquery and twitter-
    bootstrap
  * debian/watch: Update for new versioning scheme
  * debian/patches: Remove all old patches, add one patch:
    - dont-embed-libraries.patch: Don't install embedded jquery and bootstrap
      libraries
  * debian/combat: Upgrade to debhelper combat 9
  * debian/control:
    - Fix Vcs-Git field
    - Adjust debhelper version for combat level 9
    - Add jquery and bootstrap to depends for integrated webserver
    - Add python to recommends for post-processing scripts
    - Bump build-depends on libpar2-dev to support the cancel function
  * debian/links:
    - Use the system jquery and bootstrap libraries
  * debian/rules:
    - Add get-orig-source target to build modified upstream tarball
  * Adjust sample nzbget.conf:
    - Only listen to 127.0.0.1 instead of 0.0.0.0
    - Use nzbget.conf as template for webui configuration
  * Adjust sample nzbgetd init file:
    - Point to correct location of nzbget binary

 -- Andreas Moog <amoog@ubuntu.com>  Thu, 18 Jul 2013 14:50:28 +0200

nzbget (0.7.0-2) unstable; urgency=low

  * debian/control:
    - point to updated gitweb location on alioth
    - update description (Closes: #627940)
  * debian/rules:
    - build with libpar2 to enable automatic par-check/-repair
  * debian/patches/link-order.patch:
    - Libraries in LDFLAGS cause issues with ld --as-needed, put them in
      LDADD instead.
    - build with dh-autoreconf due to changes to configure.ac
  * debian/patches/ac_lang_source.patch:
    - Use AC_LANG_SOURCE macro to make sure configure is generated correctly.

 -- Andreas Moog <amoog@ubuntu.com>  Wed, 13 Jul 2011 17:06:08 +0200

nzbget (0.7.0-1) unstable; urgency=low

  * Initial release (Closes: #624381)

 -- Andreas Moog <amoog@ubuntu.com>  Fri, 06 May 2011 19:01:38 +0200
